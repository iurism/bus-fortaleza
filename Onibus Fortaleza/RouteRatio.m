//
//  RouteRatio.m
//  Onibus Fortaleza
//
//  Created by Iuri on 17/07/13.
//  Copyright (c) 2013 Iuri Mac. All rights reserved.
//

#import "RouteRatio.h"

@implementation RouteRatio

-(id)initWithCircle:(MKCircle*)circle andAnnotation:(CustomAnnotation*)annot{
    _circle = circle;
    _annotation = annot;
    return self;
}

@end
