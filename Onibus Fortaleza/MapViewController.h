//
//  ViewController.h
//  Onibus Fortaleza
//
//  Created by Iuri on 11/07/13.
//  Copyright (c) 2013 Iuri Mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "KMLParser.h"
#import "RoutesViewController.h"
#import "RouteRatio.h"

@interface MapViewController : UIViewController <MKMapViewDelegate,RoutesViewControllerDelegate>{
    

}

@property (nonatomic, assign) RouteRatio* selectedPin;

@property (nonatomic, strong) KMLParser* kmlParser;
@property (nonatomic, strong) UIView* deletePinView;
@property (weak, nonatomic) IBOutlet MKMapView *map;
@property (weak, nonatomic) IBOutlet UILabel *routeName;
@property (weak, nonatomic) IBOutlet UINavigationBar *mapNavBar;

@property (strong, nonatomic) NSMutableArray* pinsCircles;

@property (nonatomic, strong) RouteRatio* originPin;
@property (nonatomic, strong) RouteRatio* destinyPin;

@property (nonatomic, strong) NSArray* currentRoute;

-(void)drawRoute:(NSArray*)routes;

- (IBAction)teste:(id)sender;

- (IBAction)showRoutes:(id)sender;
- (IBAction)showMenu:(id)sender;

@end
