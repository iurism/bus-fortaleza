//
//  ViewController.m
//  Onibus Fortaleza
//
//  Created by Iuri on 11/07/13.
//  Copyright (c) 2013 Iuri Mac. All rights reserved.
//

#import "MapViewController.h"
#import "KMLPlacemark.h"
#import "SlidingViewController.h"
#import "CustomAnnotation.h"
#import "RouteRatio.h"
#import "CustomAnnotationView.h"
#import "KMLLineString.h"
#import "MenuViewController.h"
#import <SVProgressHUD.h>

#define CIRCLE_RADIUS 300

@interface MapViewController ()

@end

@implementation MapViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.map.delegate = self;
    self.pinsCircles = [NSMutableArray new];
    
    self.currentRoute = [NSArray new];
    
    self.routeName.font = [UIFont fontWithName:@"DS-Digital-BoldItalic" size:18];
    self.routeName.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8];
    self.routeName.textColor = [UIColor yellowColor];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // shadowPath, shadowOffset, and rotation is handled by ECSlidingViewController.
    // You just need to set the opacity, radius, and color.
    self.view.layer.shadowOpacity = 0.75f;
    self.view.layer.shadowRadius = 10.0f;
    self.view.layer.shadowColor = [UIColor blackColor].CGColor;
    
    if (![self.slidingViewController.underRightViewController isKindOfClass:[RoutesViewController class]]) {
        self.slidingViewController.underRightViewController  = [self.storyboard instantiateViewControllerWithIdentifier:@"Routes"];
        ((RoutesViewController*)self.slidingViewController.underRightViewController).delegate = self; //TODO: remove this delegate and use topViewController reference on sels.slidingViewController
        self.slidingViewController.underRightWidthLayout = ECFixedRevealWidth;
        
    }
    
    if (![self.slidingViewController.underLeftViewController isKindOfClass:[MenuViewController class]]) {
        self.slidingViewController.underLeftViewController  = [self.storyboard instantiateViewControllerWithIdentifier:@"Menu"];
        //TODO: remove this delegate and use topViewController reference on sels.slidingViewController
        self.slidingViewController.underLeftWidthLayout = ECFixedRevealWidth;
        
    }
    
    [self.view addGestureRecognizer:self.slidingViewController.panGesture];
    
    UILongPressGestureRecognizer *recognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(addPin:)];
//    [recognizer setNumberOfTapsRequired:1];
    [self.map addGestureRecognizer:recognizer];
}

-(void)viewDidAppear:(BOOL)animated{
    
    CGRect mapFrame = self.map.frame;
    
    self.deletePinView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 70, 70)];
    self.deletePinView.backgroundColor = [UIColor whiteColor];
    self.deletePinView.center = CGPointMake(self.map.center.x, mapFrame.size.height - 60);
    self.deletePinView.alpha = 0.0;
    [self.map addSubview:self.deletePinView];
}


- (void)addPin:(UITapGestureRecognizer*)recognizer
{
//    CGPoint tappedPoint = [recognizer locationInView:self.map];
//    CLLocationCoordinate2D coord = [self.map convertPoint:tappedPoint toCoordinateFromView:self.map];
//
//    
//    CustomAnnotation* annot = [[CustomAnnotation alloc]initWithCoordinate:coord];
//    annot.title = @"Iuri";
//    annot.subtitle = @"Matsuura";
//    MKCircle *circle = [MKCircle circleWithCenterCoordinate:[annot coordinate] radius:CIRCLE_RADIUS];
//    
//    RouteRatio* rRatio = [[RouteRatio alloc]initWithCircle:circle andAnnotation:annot];
//    [self.pinsCircles addObject:rRatio];
//    
//    [self.map addOverlay:rRatio.circle];
//    [self.map addAnnotation:rRatio.annotation];
    
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        CGPoint tappedPoint = [recognizer locationInView:self.map];
        CLLocationCoordinate2D coord = [self.map convertPoint:tappedPoint toCoordinateFromView:self.map];
        
        CustomAnnotation* annot = [[CustomAnnotation alloc]initWithCoordinate:coord];
        MKCircle *circle = [MKCircle circleWithCenterCoordinate:[annot coordinate] radius:CIRCLE_RADIUS];
        
        if (!self.originPin && !self.destinyPin) {
            
            annot.title = @"Origem";
            annot.subtitle = @"Sai daqui";
            self.originPin = [[RouteRatio alloc]initWithCircle:circle andAnnotation:annot];
            [self.pinsCircles addObject:self.originPin];
            
            [self.map addOverlay:self.originPin.circle];
            [self.map addAnnotation:self.originPin.annotation];
        }
        else if (self.originPin && !self.destinyPin){
            
            annot.title = @"Destino";
            annot.subtitle = @"Chega aqui";
            
            self.destinyPin = [[RouteRatio alloc]initWithCircle:circle andAnnotation:annot];
            [self.pinsCircles addObject:self.destinyPin];
            
            [self.map addOverlay:self.destinyPin.circle];
            [self.map addAnnotation:self.destinyPin.annotation];
            
            [self searchRoutes];
        }
        
        else if (!self.originPin && self.destinyPin){
            
            annot.title = @"Origem";
            annot.subtitle = @"Sai daqui";
            self.originPin = [[RouteRatio alloc]initWithCircle:circle andAnnotation:annot];
            [self.pinsCircles addObject:self.originPin];
            
            [self.map addOverlay:self.originPin.circle];
            [self.map addAnnotation:self.originPin.annotation];
            
            [self searchRoutes];
        }
    }
    
    
}


-(void)drawRoute:(NSArray*)routes{
    // Add all of the MKOverlay objects parsed from the KML file to the map.
    
    [self removePreviousRouteFromMap];

    self.currentRoute = [NSArray arrayWithArray:routes];

    NSMutableArray *overlays = [NSMutableArray new];
    
    for (KMLPlacemark* placeAUx in routes) {
        [overlays addObject:placeAUx.overlay];
        self.mapNavBar.topItem.title = [placeAUx.name substringToIndex:3];
        self.routeName.text = [placeAUx.name substringFromIndex:5];
    }
    
    [self.map addOverlays:overlays];
    
    // Add all of the MKAnnotation objects parsed from the KML file to the map.
    NSArray *annotations = [self.kmlParser points];
    [self.map addAnnotations:annotations];
    
        
    // Walk the list of overlays and annotations and create a MKMapRect that
    // bounds all of them and store it into flyTo.
    MKMapRect flyTo = MKMapRectNull;
    for (id <MKOverlay> overlay in overlays) {
        if (MKMapRectIsNull(flyTo)) {
            flyTo = [overlay boundingMapRect];
        } else {
            flyTo = MKMapRectUnion(flyTo, [overlay boundingMapRect]);
        }
    }
    
    for (id <MKAnnotation> annotation in annotations) {
        MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
        MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0, 0);
        if (MKMapRectIsNull(flyTo)) {
            flyTo = pointRect;
        } else {
            flyTo = MKMapRectUnion(flyTo, pointRect);
        }
    }
    
    // Position the map so that all overlays and annotations are visible on screen.
    self.map.visibleMapRect = flyTo;
}

- (IBAction)teste:(id)sender
{
    [SVProgressHUD showWithStatus:@"Calculando rotas próximas..." maskType:SVProgressHUDMaskTypeBlack];
    [self performSelectorInBackground:@selector(intersectPlacemarksForAllPins) withObject:nil];
}

-(void)searchRoutes{
    [SVProgressHUD showWithStatus:@"Calculando rotas próximas..." maskType:SVProgressHUDMaskTypeBlack];
    [self performSelectorInBackground:@selector(intersectPlacemarksForAllPins) withObject:nil];
}


-(void)intersectPlacemarksForAllPins
{
    RoutesViewController *routesViewController = (RoutesViewController*)self.slidingViewController.underRightViewController;

    NSMutableArray *arrayOfSets = [NSMutableArray new];
    
    for (RouteRatio *pin in self.pinsCircles) {
        [arrayOfSets addObject:[self nearPlacemarksForCoordinate:pin.annotation.coordinate]];
    }
    
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        NSMutableSet *filteredPlacemarks;
        if (arrayOfSets.count > 0)
        {
            filteredPlacemarks = arrayOfSets[0];
            for (NSUInteger i = 1; i < arrayOfSets.count; i++) {
                [filteredPlacemarks intersectSet:arrayOfSets[i]];
            }
            
            if (filteredPlacemarks.count > 0)
            {
                [SVProgressHUD showSuccessWithStatus:@"Rotas calculadas com sucesso!"];
                
                routesViewController.searchResults = [[filteredPlacemarks allObjects] mutableCopy];
                [routesViewController.tableView reloadData];
                
                [self.slidingViewController anchorTopViewTo:ECLeft];
            }
            else
            {
                [SVProgressHUD showErrorWithStatus:@"Nenhuma rota encontrada!"];
            }
        }
        else
        {
            [SVProgressHUD showSuccessWithStatus:@"Nenhum pino posicionado! Todas as rotas serão listadas"];
            
            [routesViewController.searchResults removeAllObjects];
            [routesViewController.tableView reloadData];
            
            [self.slidingViewController anchorTopViewTo:ECLeft];
        }

    });
    
      

}

-(NSMutableSet*)nearPlacemarksForCoordinate:(CLLocationCoordinate2D)coordinate
{
    RoutesViewController *routesViewController = (RoutesViewController*)self.slidingViewController.underRightViewController;
    
    NSMutableSet *filteredPlacemarks = [NSMutableSet new];
    
    //loop through all placemarks
    NSArray *allValues = routesViewController.placemarkers.allValues;
    for (NSArray *placemarks in allValues)
    {
        
        for (KMLPlacemark *placemark in placemarks) {
            
            KMLLineString *routePath = (KMLLineString*)placemark.geometry;
            
            //loop through all coordinates of that placemark (route of the bus)
            NSUInteger i;
            for (i = 0; i < routePath.length; i++)
            {
                CLLocationCoordinate2D pointOfPath = routePath.points[i];
                CLLocation *pointOfPathLocation = [[CLLocation alloc] initWithLatitude:pointOfPath.latitude longitude:pointOfPath.longitude];
                
                CLLocation *myPinLocation = [[CLLocation alloc] initWithLatitude:coordinate.latitude longitude:coordinate.longitude];
                CGFloat distanceFromLocation = [pointOfPathLocation distanceFromLocation:myPinLocation];
                
                if (distanceFromLocation < CIRCLE_RADIUS) {
                    [filteredPlacemarks addObject:placemarks];
                    break;
                }
            }
            
            if (i < routePath.length) {
                break;
            }
        }
    }
    
    
    return filteredPlacemarks;    
}


- (IBAction)showRoutes:(id)sender {
    [self.slidingViewController anchorTopViewTo:ECLeft];
}

- (IBAction)showMenu:(id)sender {
    [self.slidingViewController anchorTopViewTo:ECRight];
}

-(void)loadRoutes:(NSArray *)routes{
    [self drawRoute:routes];
}

-(void)removePreviousRouteFromMap{
    for (KMLPlacemark* placeMark in self.currentRoute) {
        [self.map removeOverlay:placeMark.overlay];
    }
}

#pragma mark MKMapViewDelegate

- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id <MKOverlay>)overlay
{
    if ([overlay isKindOfClass:[MKCircle class]]) {
        MKCircleView* circleView = [[MKCircleView alloc]initWithCircle:overlay];
        circleView.fillColor = [UIColor colorWithRed:0.06 green:0.41 blue:0.62 alpha:0.6];
        circleView.strokeColor = [UIColor colorWithRed:0.0 green:0.0 blue:1.0 alpha:1.0];
        [circleView setLineWidth:2.5];
        return circleView;
    }
    
    return [self.kmlParser viewForOverlay:overlay];
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[CustomAnnotation class]]) {

        MKPinAnnotationView* viewAux = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:@"CustomAnnotation"];
        if (viewAux == nil) {
            viewAux = [[MKPinAnnotationView alloc] initWithAnnotation: annotation reuseIdentifier: @"CustomAnnotation"]; 
        }
        viewAux.annotation = annotation;

        viewAux.canShowCallout = YES;
        viewAux.draggable = YES;
        return viewAux;
        
    }
    return [self.kmlParser viewForAnnotation:annotation];
}


- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)annotationView didChangeDragState:(MKAnnotationViewDragState)newState fromOldState:(MKAnnotationViewDragState)oldState
{
    
    CLLocationCoordinate2D droppedAt = annotationView.annotation.coordinate;    

    if (newState == MKAnnotationViewDragStateStarting) {
        self.deletePinView.alpha = 1.0;
        for (RouteRatio* rRatioAux in self.pinsCircles) {
            if (rRatioAux.annotation == annotationView.annotation) {
                self.selectedPin = rRatioAux;
                [self.map removeOverlay:rRatioAux.circle];
                rRatioAux.circle = nil;
                
                NSLog(@"%@",rRatioAux.circle);
                
            }
        }
    }

    if (newState == MKAnnotationViewDragStateCanceling) {
                
        for (RouteRatio* rRatioAux in self.pinsCircles) {
            if (rRatioAux.annotation == annotationView.annotation) {
                [self.map removeOverlay:rRatioAux.circle];
                rRatioAux.circle = nil;
                rRatioAux.circle = [MKCircle circleWithCenterCoordinate:droppedAt radius:CIRCLE_RADIUS];
                [self.map addOverlay:rRatioAux.circle];
                
                NSLog(@"%@",rRatioAux.circle);
                self.deletePinView.alpha = 0.0;

            }
        }
    }
    
    if (newState == MKAnnotationViewDragStateEnding || newState == oldState)
    {
        NSLog(@"Pin dropped at %f,%f", droppedAt.latitude, droppedAt.longitude);

        NSLog(@"%@",self.selectedPin.circle);

        self.selectedPin.circle = [MKCircle circleWithCenterCoordinate:droppedAt radius:CIRCLE_RADIUS];
        [self.map addOverlay:self.selectedPin.circle];
        NSLog(@"%@",self.selectedPin.circle);
        NSLog(@"%@",NSStringFromCGRect([self.map viewForAnnotation:self.selectedPin.annotation].frame));
        NSLog(@"%@",NSStringFromCGRect(self.deletePinView.frame));

//        if (CGRectIntersectsRect([self.map viewForAnnotation:self.selectedPin.annotation].frame,self.deletePinView.frame)) {
//            [self searchRoutes];
//        }

    }
    
}

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint location = [touch locationInView:self.map];
    if(CGRectContainsPoint(self.deletePinView.frame, location)) {
        self.deletePinView.backgroundColor = [UIColor redColor];
    }
    else{
        self.deletePinView.backgroundColor = [UIColor whiteColor];
    }
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint location = [touch locationInView:self.map];
    if(CGRectContainsPoint(self.deletePinView.frame, location) && self.selectedPin) {
        [self.map removeAnnotation:self.selectedPin.annotation];
        [self.map removeOverlay:self.selectedPin.circle];
        [self.pinsCircles removeObject:self.selectedPin];
        
        [self clearPinWithObject:self.selectedPin];
        self.selectedPin = nil;
        
        self.deletePinView.backgroundColor = [UIColor whiteColor];
    }
    else{
        [self searchRoutes];
    }
    self.deletePinView.alpha = 0.0;
}

-(void)clearPinWithObject:(RouteRatio*)rRatio{
    if (rRatio == self.originPin) {
        self.originPin = nil;
    }
    
    else{
        self.destinyPin = nil;
    }
}

@end
