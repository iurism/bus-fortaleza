//
//  CustomAnnotation.m
//  Onibus Fortaleza
//
//  Created by Iuri on 17/07/13.
//  Copyright (c) 2013 Iuri Mac. All rights reserved.
//

#import "CustomAnnotation.h"

@implementation CustomAnnotation

@synthesize coordinate = _coordinate;



-(id)initWithCoordinate:(CLLocationCoordinate2D)coord {
    _coordinate = coord;
    return self;
}

-(CLLocationCoordinate2D)coordinate
{
    return _coordinate;
}

- (void)setCoordinate:(CLLocationCoordinate2D)newCoordinate {
    _coordinate = newCoordinate;
}

@end
