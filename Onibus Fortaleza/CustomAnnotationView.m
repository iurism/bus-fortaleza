//
//  CustomAnnotationView.m
//  Onibus Fortaleza
//
//  Created by Iuri on 18/07/13.
//  Copyright (c) 2013 Iuri Mac. All rights reserved.
//

#import "CustomAnnotationView.h"

@implementation CustomAnnotationView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


-(id)initWithAnnotation:(id<MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    if (self != nil)
    {
        CGRect frame = self.frame;
        frame.size = CGSizeMake(30, 30);
        self.frame = frame;
        self.backgroundColor = [UIColor redColor];
        
        
    }
    return self;
}


- (void)drawRect:(CGRect)rect
{
    // Drawing code
}


@end
