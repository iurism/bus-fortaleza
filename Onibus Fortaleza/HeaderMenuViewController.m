//
//  HeaderMenuViewController.m
//  Onibus Fortaleza
//
//  Created by Iuri on 04/08/13.
//  Copyright (c) 2013 Iuri Mac. All rights reserved.
//

#import "HeaderMenuViewController.h"

@interface HeaderMenuViewController ()

@end

@implementation HeaderMenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.headerTitle.text = @"Sobre";
    self.iconImageView.image = [UIImage imageNamed:@"info"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
