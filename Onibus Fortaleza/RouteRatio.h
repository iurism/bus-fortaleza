//
//  RouteRatio.h
//  Onibus Fortaleza
//
//  Created by Iuri on 17/07/13.
//  Copyright (c) 2013 Iuri Mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "CustomAnnotation.h"

@interface RouteRatio : NSObject

@property (nonatomic,strong) MKCircle* circle;
@property (nonatomic,strong) CustomAnnotation* annotation;

-(id)initWithCircle:(MKCircle*)circle andAnnotation:(CustomAnnotation*)annot;

@end
