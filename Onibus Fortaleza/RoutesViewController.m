//
//  RoutesViewController.m
//  Onibus Fortaleza
//
//  Created by Iuri on 12/07/13.
//  Copyright (c) 2013 Iuri Mac. All rights reserved.
//

#import "RoutesViewController.h"
#import "MapViewController.h"
#import "KMLPlacemark.h"

@interface RoutesViewController ()

@end

@implementation RoutesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    _placemarkArray = [NSMutableArray new];
    _placemarkers = [NSMutableDictionary new];
    _searchResults = [NSMutableArray new];
    
    [self parseKMLFile];
    [self sortPlacemarks];
    
    [self.slidingViewController setAnchorLeftRevealAmount:280.0f];
    self.slidingViewController.underRightWidthLayout = ECFullWidth;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)parseKMLFile{
    
    // Locate the path to the route.kml file in the application's bundle
    // and parse it with the KMLParser.
    NSString *path = [[NSBundle mainBundle] pathForResource:@"todas" ofType:@"kml"];
    NSURL *url = [NSURL fileURLWithPath:path];
    self.kmlParser = [[KMLParser alloc] initWithURL:url];
    [self.kmlParser parseKML];
}

-(void)sortPlacemarks{
    
    KMLPlacemark* ant = self.kmlParser.placemarkss[0];
    [_placemarkArray addObject:ant];
    
    for (int i = 1; i<self.kmlParser.placemarkss.count; i++) {
        
        KMLPlacemark* actual = self.kmlParser.placemarkss[i];
        NSString* antName = [ant.name substringToIndex:3];
        NSString* actualName = [actual.name substringToIndex:3];
        
        if ([actualName isEqualToString:antName]) {
            
            [_placemarkArray addObject:actual];
            ant = actual;
        }
        else{
            NSArray* array = [[NSArray alloc] initWithArray:_placemarkArray];
            [_placemarkers setObject:array forKey:ant.name];
            ant = actual;
            [_placemarkArray removeAllObjects];
            [_placemarkArray addObject:ant];
        }
    }
    
    [self sortPlacemarksKeys];
    
}

-(void)sortPlacemarksKeys{

    self.sortedKeys = [[_placemarkers allKeys] sortedArrayUsingComparator:^(id firstObject, id secondObject) {
        return [((NSString *)firstObject) compare:((NSString *)secondObject) options:NSNumericSearch];
    }];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return (_searchResults.count > 0 ) ? _searchResults.count : _placemarkers.allKeys.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    NSArray* places;
    
    if (_searchResults.count > 0) {
        places = [_searchResults objectAtIndex:indexPath.row];
    }
    else{
        places = [_placemarkers objectForKey:self.sortedKeys[indexPath.row]];
    }

    KMLPlacemark* placeM = places[0];
    
    cell.textLabel.font = [UIFont fontWithName:@"Helvetica" size:14];
    cell.textLabel.text = placeM.name;
    cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    cell.textLabel.textColor = [UIColor yellowColor];
    cell.textLabel.backgroundColor = [UIColor grayColor];
    cell.contentView.backgroundColor = [UIColor grayColor ];
    return cell;
}

//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    return 68;
//}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    MapViewController* mapVC = (MapViewController*)self.slidingViewController.topViewController;
    
    mapVC.kmlParser = self.kmlParser;
    
    
    if (_searchResults.count > 0) {
        [self.delegate loadRoutes:[_searchResults objectAtIndex:indexPath.row]];
    }
    else{
        NSArray* routes = [_placemarkers objectForKey:self.sortedKeys[indexPath.row]];
        [self.delegate loadRoutes:routes];
    }
    
    
    [self.slidingViewController resetTopView];
}

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    [_searchResults removeAllObjects];

    for (NSString *key in _placemarkers.allKeys) {
        
        if ([key rangeOfString:searchText options:NSCaseInsensitiveSearch].location != NSNotFound)
        {
            [_searchResults addObject:_placemarkers[key]];
        }
    }

    NSLog(@"%d",_searchResults.count);
}


-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:[self.searchDisplayController.searchBar
                                                     selectedScopeButtonIndex]]];
    
    return YES;
}
@end
