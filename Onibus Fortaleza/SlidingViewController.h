//
//  PaperViewController.h
//  Onibus Fortaleza
//
//  Created by Iuri on 13/07/13.
//  Copyright (c) 2013 Iuri Mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RoutesViewController.h"
#import "MapViewController.h"

#import "ECSlidingViewController.h"

@interface SlidingViewController : ECSlidingViewController 

@property (strong, nonatomic) RoutesViewController* routesVC;
@property (strong, nonatomic) MapViewController* mapVC;

@end
