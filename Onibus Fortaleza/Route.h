//
//  Route.h
//  Onibus Fortaleza
//
//  Created by Iuri on 12/07/13.
//  Copyright (c) 2013 Iuri Mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Route : NSObject

@property (nonatomic, assign) NSInteger identifier;
@property (nonatomic, strong) NSString*  name;

@end
