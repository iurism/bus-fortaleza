//
//  RoutesViewController.h
//  Onibus Fortaleza
//
//  Created by Iuri on 12/07/13.
//  Copyright (c) 2013 Iuri Mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KMLParser.h"
#import "ECSlidingViewController.h"

@protocol RoutesViewControllerDelegate <NSObject>

-(void)loadRoutes:(NSArray*)routes;

@end

@interface RoutesViewController : UIViewController <UITableViewDataSource,UITableViewDelegate,UISearchDisplayDelegate>{
    
    NSMutableArray* _placemarkArray;
}

@property (strong, nonatomic)  KMLParser *kmlParser;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSMutableDictionary* placemarkers;
@property (strong, nonatomic) NSMutableArray* searchResults;


@property (strong, nonatomic) NSArray* sortedKeys;

@property (weak, nonatomic) id<RoutesViewControllerDelegate> delegate;


@end
