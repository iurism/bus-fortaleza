//
//  CustomAnnotationView.h
//  Onibus Fortaleza
//
//  Created by Iuri on 18/07/13.
//  Copyright (c) 2013 Iuri Mac. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface CustomAnnotationView : MKAnnotationView

@property (nonatomic,strong)UIView* backgroundView;

@end
