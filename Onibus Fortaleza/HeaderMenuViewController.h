//
//  HeaderMenuViewController.h
//  Onibus Fortaleza
//
//  Created by Iuri on 04/08/13.
//  Copyright (c) 2013 Iuri Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HeaderMenuViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *headerTitle;

@end
