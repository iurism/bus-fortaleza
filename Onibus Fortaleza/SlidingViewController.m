//
//  PaperViewController.m
//  Onibus Fortaleza
//
//  Created by Iuri on 13/07/13.
//  Copyright (c) 2013 Iuri Mac. All rights reserved.
//

#import "SlidingViewController.h"


@implementation SlidingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.topViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Map"];
}

@end
